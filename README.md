# Adonis fullstack application

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.

## PostgreSQL Docker

Create container `PostgreSQL` with the following command:

```bash
$ docker run -p 5432:5432 \
  -e "POSTGRES_PASSWORD=p@ssw0rd24" \
  -e "POSTGRES_USER=root" \
  -e "POSTGRES_DB=adonis-tasks" \
  --name adonis-pg \
  -d postgres
```


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
